<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" indent="yes" />
	<xsl:template match="rootElement">
		<commits>
			<xsl:apply-templates select="commits" />
		</commits>
	</xsl:template>
	<xsl:template match="commits">
		<commit>
			<xsl:choose>
				<xsl:when test="contains(message, '[Tweet]')">
					<header>Yo! Look at my genius commit!</header>
					<post>true</post>
					<msg>
						<xsl:call-template name="removePrefix">
							<xsl:with-param name="value" select="message" />
						</xsl:call-template>
					</msg>
				</xsl:when>
				<xsl:otherwise>
					<post>false</post>
					<msg>
						<xsl:value-of select="message" />
					</msg>
				</xsl:otherwise>
			</xsl:choose>
			<username>
				<xsl:value-of select="author/username" />
			</username>
			<timestamp>
				<xsl:value-of select="timestamp" />
			</timestamp>
			<hash>
				<xsl:value-of select="id" />
			</hash>
			<url>
				<xsl:value-of select="url" />
			</url>
		</commit>
	</xsl:template>
	<xsl:template name="removePrefix">
		<xsl:param name="value" />
		<xsl:value-of select="substring-after($value, '[Tweet]')" />
	</xsl:template>
</xsl:stylesheet>
